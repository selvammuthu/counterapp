# Based on ubuntu / Tomcat 7 base
# EXPERIMENTAL - NOT FOR PRODUCTION USE!
FROM muthuselvam/tomcat7:latest
MAINTAINER Muthu Selvam <selvammuthu19@gmail.com>

# Install our tomcat WAR file
ADD target/*.war /opt/apache-tomcat-${TOMCAT_VERSION}/webapps/counter.war

CMD $CATALINA_HOME/bin/catalina.sh run